FROM node:18-alpine

USER root
## user permission diferent of root can block some resouces and commands in docker execution

WORKDIR /home/node/app

COPY package*.json .

RUN npm install

USER node

EXPOSE 3004

##CMD [ "sh", "-c", "npm install" ]
