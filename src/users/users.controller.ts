import { Body, Controller, Get, Post } from '@nestjs/common';
import { User } from './interfaces/user.interface';
import { UsersService } from './users.service';
import { ApiBody } from '@nestjs/swagger';
import { UserDto } from './dtos/user.dto';

@Controller('users')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Post()
  @ApiBody({ type: UserDto })
  createUser(@Body() user: UserDto): User {
    return this.userService.create(user);
  }

  @Get()
  findUsers(): User[] {
    return this.userService.findAll();
  }
}
